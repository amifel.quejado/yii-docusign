<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tokens".
 *
 * @property int $id
 * @property string $auth_code
 * @property string|null $access_token
 * @property string|null $refresh_token
 * @property string|null $date_auth_code
 * @property string|null $date_access_token
 * @property int $exp_time
 * @property string|null $user_name
 * @property string|null $user_email
 * @property string|null $account_id
 * @property string|null $account_name
 * @property string|null $basepath
 */
class Token extends \yii\db\ActiveRecord
{
  /**
   * {@inheritdoc}
   */
  public static function tableName()
  {
    return 'tokens';
  }

  /**
   * {@inheritdoc}
   */
  public function rules()
  {
    return [
      [['auth_code'], 'required'],
      [['id', 'exp_time'], 'integer'],
      [['date_auth_code', 'date_access_token'], 'safe'],
      [['auth_code', 'access_token', 'refresh_token'], 'string', 'max' => 255],
      [['id'], 'unique'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function attributeLabels()
  {
    return [
      'id' => 'ID',
      'auth_code' => 'Auth Code',
      'access_token' => 'Access Token',
      'refresh_token' => 'Refresh Token',
      'date_auth_code' => 'Date Auth Code',
      'date_access_token' => 'Date Access Token',
    ];
  }

  public static function model($className = __CLASS__)
  {
    return parent::model($className);
  }

  /**
   * {@inheritdoc}
   * @return TokensQuery the active query used by this AR class.
   */
  public static function find()
  {
    return new TokensQuery(get_called_class());
  }
}
