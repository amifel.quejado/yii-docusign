<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%tokens}}`.
 */
class m220404_074701_create_tokens_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tokens}}', [
            'id' => 'INT PRIMARY KEY AUTO_INCREMENT',
            'auth_code' => 'TEXT NOT NULL',
            'access_token' => 'TEXT NULL',
            'refresh_token' => 'TEXT NULL',
            'date_auth_code' => 'DATETIME NULL DEFAULT NOW()',
            'date_access_token' => 'DATETIME',
            'exp_time' => 'INT',
            'user_name' => 'VARCHAR(255) NULL',
            'user_email' => 'VARCHAR(255) NULL',
            'account_id' => 'VARCHAR(255) NULL',
            'account_name' => 'VARCHAR(255) NULL',
            'basepath' => 'VARCHAR(255) NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%tokens}}');
    }
}
